#include "display.h"
#include "game.h"

void menu(std::stack <card>& deck) {
     display_solitaire();
     char mode = display_and_choose_mode();
     game(deck, mode);
}

void display_solitaire() {
     std::cout << "------------------------------------------------------------------------------------------------------------------------" << std::endl;
     std::cout << "-------------------------------------------------------SOLITAIRE--------------------------------------------------------" << std::endl;
     std::cout << "------------------------------------------------------------------------------------------------------------------------" << std::endl;
     std::cout << "                                                Press Enter to continue " << std::endl;
     while (std::cin.get() != '\n');
     system("CLS");
}

char display_and_choose_mode() {
     char mode = '0';
     do {
          SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
          std::cout << "Choose game mode (type 1 or 2):" << std::endl;
          std::cout << "1.Deal one card at a time" << std::endl;
          std::cout << "2.Deal three card at a time" << std::endl;
          std::cin >> mode;
          system("CLS");
     } while (mode != '1' && mode != '2');
     return mode;
}

void display_board(std::stack <card> open_deck, std::stack<card> winnig_stack[], std::vector<card> cards_columns[], char mode) {
     (mode == '1') ? display_upper_row_deal_one(open_deck, winnig_stack) : display_upper_row_deal_three(open_deck, winnig_stack);
     display_cards_columns(cards_columns);
}

void display_upper_row_deal_one(std::stack <card> open_deck, std::stack<card> winnig_stack[]) {
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
     std::cout << " #####  ";
     if (!open_deck.empty()) {
          check_and_change_color(open_deck.top().color);
          std::cout << open_deck.top().color << " " << open_deck.top().fiugre << open_deck.top().symbol;
     }
     else {
          std::cout << "     ";
     }
     std::cout << "                                        ";
     for (int i = 0; i < 4; i++) {
          if (!winnig_stack[i].empty() && winnig_stack[i].top().visible == true) {
               check_and_change_color(winnig_stack[i].top().color);
               std::cout << winnig_stack[i].top().color << " " << winnig_stack[i].top().fiugre << winnig_stack[i].top().symbol << "  ";
          }
          else {
               SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
               std::cout << " ##### ";
          }
     }
     std::cout << std::endl;
}

void display_upper_row_deal_three(std::stack <card> open_deck, std::stack<card> winnig_stack[]) {
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
     std::stack <card> three_card_deck;
     int cards_to_take = 0;
     std::cout << " #####  ";
     (open_deck.size() >= 3) ? cards_to_take = 3 : cards_to_take = open_deck.size();
     for (int i = 0; i < cards_to_take; i++) {
          three_card_deck.push(open_deck.top());
          open_deck.pop();
     }
     for (int i = 0; i < 3; i++) {
          if (!three_card_deck.empty()) {
               check_and_change_color(three_card_deck.top().color);
               std::cout << three_card_deck.top().color << " " << three_card_deck.top().fiugre << three_card_deck.top().symbol << "  ";
               three_card_deck.pop();
          }
          else {
               std::cout << "     ";
          }
     }
     std::cout << "                           ";
     for (int i = 0; i < 4; i++) {
          if (!winnig_stack[i].empty() && winnig_stack[i].top().visible == true) {
               check_and_change_color(winnig_stack[i].top().color);
               std::cout << winnig_stack[i].top().color << " " << winnig_stack[i].top().fiugre << winnig_stack[i].top().symbol << "  ";
          }
          else {
               SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
               std::cout << " ##### ";
          }
     }
     std::cout << std::endl;
}

void display_cards_columns(std::vector<card> cards_columns[]) {
     int longest_vector = 0;
     for (int i = 0; i < 7; i++) {
          if (longest_vector < cards_columns[i].size()) {
               longest_vector = cards_columns[i].size();
          }
     }
     std::cout << "          ";
     for (int i = 0; i < 7; i++) {
          if (i < 6)std::cout << i + 1 << "          ";
          else std::cout << i + 1 << "     ";
     }
     std::cout << std::endl;
     for (int i = 0; i < longest_vector; i++) {
          SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
          if (i < 10) std::cout << i + 1 << ".   ";
          else std::cout << i << ".  ";
          for (int j = 0; j < 7; j++) {
               if (cards_columns[j].size() > i) {
                    if (cards_columns[j][i].visible == true) {
                         check_and_change_color(cards_columns[j][i].color);
                         if (cards_columns[j][i].fiugre.length() < 2) {
                              std::cout << "   " << cards_columns[j][i].color << " " << cards_columns[j][i].fiugre << cards_columns[j][i].symbol << "    ";
                         }
                         else {
                              std::cout << "   " << cards_columns[j][i].color << " " << cards_columns[j][i].fiugre << cards_columns[j][i].symbol << "   ";
                         }
                    }
                    else {
                         SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0);
                         std::cout << "   #####   ";
                    }
               }
               else {
                    std::cout << "           ";
               }
          }
          std::cout << std::endl;
     }
}
char display_possible_movements(std::stack <card> open_deck, std::stack<card> winnig_stack[], std::vector<card> cards_columns[], char mode) {
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
     char choose_move = '0';
     do {
          std::cin.clear();
          std::cin.ignore(100, '\n');
          system("CLS");
          display_board(open_deck, winnig_stack, cards_columns, mode);
          SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
          std::cout << "Choose move you do (type 1, 2, 3, 4, 5, 6, 7 or 8):" << std::endl;
          std::cout << "1.Move from deck to columns" << std::endl;
          std::cout << "2.Move form column to columm" << std::endl;
          std::cout << "3.Move from column to winnig stack" << std::endl;
          std::cout << "4.Move form deck to winnig stack" << std::endl;
          std::cout << "5.Move from winnig stack to column" << std::endl;
          std::cout << "6.New card from deck" << std::endl;
          std::cout << "7.Deal New Game" << std::endl;
          std::cout << "8.Quit Game" << std::endl;
          std::cin >> choose_move;
     } while (choose_move != '1' && choose_move != '2' && choose_move != '3' && choose_move != '4' && choose_move != '5' && choose_move != '6' && choose_move != '7' && choose_move != '8');
     system("CLS");
     return choose_move;
}

void check_and_change_color(char color) {
     (color == 'B') ? SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF0) : SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xFC);
}

void display_impossible_move(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     display_board(open_deck, winnig_stack, cards_columns, mode);
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
     std::cout << "Impossible move, choose different move" << std::endl;
}
