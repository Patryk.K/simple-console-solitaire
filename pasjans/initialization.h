#pragma once
#include "card_struct.h"


std::stack <card> init_deck();
void create_deck(std::vector<card>& deck);
void print_deck(std::stack<card> deck);
void set_figure(card& new_card, int i);
card create_new_card(int i, int change_color);
std::stack <card> rewrite_deck(std::vector <card>& deck_in_vector);