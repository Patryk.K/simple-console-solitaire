#pragma once
#include "display.h"

void game(std::stack <card>& deck, char mode);
void move_card_from_deck_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
void put_card_on_column_from_deck(std::stack<card>& open_deck, std::vector<card> cards_columns[], int& column);
void move_card_from_column_to_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
void put_card_on_winnig_stack_from_column(std::vector<card> cards_columns[], std::stack<card> winnig_stack[], int which_stack, int& column);
void move_card_from_winnig_stack_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
void move_cards_from_deck_to_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[]);
void move_card_from_column_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
void put_from_one_column_to_other(std::vector<card> cards_columns[], int& first_column, int& second_column, int& number_of_cards);
void put_card_from_closed_deck_to_open_deck(std::stack<card>& closed_deck, std::stack<card>& open_deck, char mode);
int display_and_choose_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
int display_and_choose_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
int display_and_choose_number_of_moved_cards();
bool check_if_can_put_card(card first_card, card second_card);
void fill_cards_columns(std::vector<card> cards_columns[], std::stack <card>& deck);
void check_and_change_color(char color);
int symbol_map_dictionary(char symbol);