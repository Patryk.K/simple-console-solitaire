#pragma once
#include "card_struct.h"
#include "initialization.h"


void menu(std::stack <card>& deck);
void display_solitaire();
char display_and_choose_mode();
void display_upper_row_deal_one(std::stack <card> open_deck, std::stack<card> winnig_stack[]);
void display_upper_row_deal_three(std::stack <card> open_deck, std::stack<card> winnig_stack[]);
void display_cards_columns(std::vector<card> cards_columns[]);
void display_board(std::stack <card> closed_deck, std::stack<card> winnig_stack[], std::vector<card> cards_columns[], char mode);
char display_possible_movements(std::stack <card> open_deck, std::stack<card> winnig_stack[], std::vector<card> cards_columns[], char mode);
void display_impossible_move(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode);
void check_and_change_color(char color);