#include "initialization.h"



std::stack <card> init_deck() {
     std::vector <card> deck_in_vector;
     create_deck(deck_in_vector);
     std::stack <card> deck = rewrite_deck(deck_in_vector);
     return deck;
}

std::stack <card> rewrite_deck(std::vector <card>& deck_in_vector) {
     std::stack <card> deck;
     for (int i = 0; i < deck_in_vector.size(); i++) {
          deck.push(deck_in_vector[i]);
     }
     deck_in_vector.clear();
     std::vector<card>().swap(deck_in_vector);
     return deck;
}

void create_deck(std::vector<card>& deck) {
     int change_color = 0;
     for (int i = 0; i < 52; i++) {
          if (i % 13 == 0) {
               change_color++;
          }
          card new_card = create_new_card(i, change_color);
          deck.push_back(new_card);
     }
     std::srand(time(0));
     random_shuffle(deck.begin(), deck.end());
     std::srand(time(0));
     random_shuffle(deck.begin(), deck.end());
}

card create_new_card(int i, int change_color) {
     card new_card;
     new_card.value = 1 + i % 13;
     set_figure(new_card, i);
     switch (change_color) {
     case 1: {
          new_card.color = 'R';
          new_card.symbol = 'H';
          break;
     }
     case 2: {
          new_card.color = 'R';
          new_card.symbol = 'D';
          break;
     }
     case 3: {
          new_card.color = 'B';
          new_card.symbol = 'C';
          break;
     }
     case 4: {
          new_card.color = 'B';
          new_card.symbol = 'S';
          break;
     }
     }
     return new_card;
}

void set_figure(card& new_card, int i) {
     switch (i % 13) {
     case 0: new_card.fiugre = "A"; break;
     case 9: new_card.fiugre = "10"; break;
     case 10: new_card.fiugre = "J"; break;
     case 11: new_card.fiugre = "Q"; break;
     case 12: new_card.fiugre = "K"; break;
     default: new_card.fiugre = (i % 13) + 49; break;
     }
}

void print_deck(std::stack<card> deck) {
     while (!deck.empty())
     {
          std::cout << deck.top().color << "  " << deck.top().fiugre << deck.top().symbol << std::endl;
          deck.pop();
     }
     std::cout << '\n';
}