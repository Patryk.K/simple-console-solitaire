#include "game.h"

void game(std::stack <card>& closed_deck, char mode) {
     std::stack<card> winnig_stack[4];
     std::vector<card> cards_columns[7];
     std::stack<card> open_deck;
     bool endgame = false;
     bool win = false;
     bool deal_new = false;
     fill_cards_columns(cards_columns, closed_deck);
     put_card_from_closed_deck_to_open_deck(closed_deck, open_deck, mode);

     while (!endgame) {
          char choosen_move = display_possible_movements(open_deck, winnig_stack, cards_columns, mode);
          switch (choosen_move) {
          case '1': {
               move_card_from_deck_to_column(open_deck, cards_columns, winnig_stack, mode);
               break;
          }
          case '2': {
               move_card_from_column_to_column(open_deck, cards_columns, winnig_stack, mode);
               break;
          }
          case '3': {
               move_card_from_column_to_winnig_stack(open_deck, cards_columns, winnig_stack, mode);
               break;
          }
          case '4': {
               move_cards_from_deck_to_winnig_stack(open_deck, cards_columns, winnig_stack);
               break;
          }
          case '5': {
               move_card_from_winnig_stack_to_column(open_deck, cards_columns, winnig_stack, mode);
               break;
          }
          case '6': {
               put_card_from_closed_deck_to_open_deck(closed_deck, open_deck, mode);
               break;
          }
          case '7': {
               endgame = true;
               deal_new = true;
               break;
          }
          case '8': {
               endgame = true;
               break;
          }
          }
          if (!winnig_stack[0].empty() && !winnig_stack[1].empty() && !winnig_stack[2].empty() && !winnig_stack[3].empty()) {
               if (winnig_stack[0].top().value == 13 && winnig_stack[1].top().value == 13 && winnig_stack[2].top().value == 13 && winnig_stack[3].top().value == 13) {
                    endgame = true;
                    win = true;
                    std::cout << "You won the game" << std::endl;
               }
          }
     }
     if (deal_new) {
          std::stack <card> deck = init_deck();
          menu(deck);
     }

}
void put_card_from_closed_deck_to_open_deck(std::stack<card>& closed_deck, std::stack<card>& open_deck, char mode) {
     if (mode == '1') {
          if (closed_deck.size() > 0)
          {
               open_deck.push(closed_deck.top());
               closed_deck.pop();
          }
          else
          {
               int size = open_deck.size();
               for (int i = 0; i < size; i++) {
                    closed_deck.push(open_deck.top());
                    open_deck.pop();
               }
          }
     }
     else {
          for (int i = 0; i < 3; i++) {
               if (closed_deck.size() > 0)
               {
                    open_deck.push(closed_deck.top());
                    closed_deck.pop();
                    if (closed_deck.empty()) {
                         break;
                    }
               }
               else
               {
                    int size = open_deck.size();
                    for (int i = 0; i < size; i++) {
                         closed_deck.push(open_deck.top());
                         open_deck.pop();
                    }
                    break;
               }
          }
     }
     system("CLS");
}

void move_card_from_deck_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     int column = 0;
     do {
          column = display_and_choose_column(open_deck, cards_columns, winnig_stack, mode);
          if (column < 8) {
               if (!open_deck.empty() && !cards_columns[column - 1].empty()) {
                    if (check_if_can_put_card(open_deck.top(), cards_columns[column - 1].back())) {
                         put_card_on_column_from_deck(open_deck, cards_columns, column);
                    }
                    else {
                         display_board(open_deck, winnig_stack, cards_columns, mode);
                         std::cout << "Move impossible, choose different move" << std::endl;
                    }
               }
               else if (!open_deck.empty() && cards_columns[column - 1].empty()) {
                    if (open_deck.top().value == 13) {
                         put_card_on_column_from_deck(open_deck, cards_columns, column);
                    }
               }
               else {
                    display_board(open_deck, winnig_stack, cards_columns, mode);
                    std::cout << "Deck or choosen column is empty" << std::endl;
               }
          }
     } while (column != 8);
}

void put_card_on_column_from_deck(std::stack<card>& open_deck, std::vector<card> cards_columns[], int& column) {
     cards_columns[column - 1].push_back(open_deck.top());
     cards_columns[column - 1].back().visible = true;
     open_deck.pop();
     column = 8;
}


void move_card_from_column_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     int first_column = 0;
     int number_of_cards = 0;
     int second_column = 0;
     std::stack<card> cards_buffer;
     do {
          first_column = display_and_choose_column(open_deck, cards_columns, winnig_stack, mode);
          if (first_column < 8) {
               display_board(open_deck, winnig_stack, cards_columns, mode);
               number_of_cards = display_and_choose_number_of_moved_cards();
               second_column = display_and_choose_column(open_deck, cards_columns, winnig_stack, mode);
               if (second_column < 8) {
                    if (number_of_cards <= cards_columns[first_column - 1].size() && number_of_cards > 0) {
                         if (!cards_columns[first_column - 1].empty() && !cards_columns[second_column - 1].empty()) {
                              if (cards_columns[first_column - 1][cards_columns[first_column - 1].size() - number_of_cards].visible == true) {
                                   if (check_if_can_put_card(cards_columns[first_column - 1][cards_columns[first_column - 1].size() - number_of_cards], cards_columns[second_column - 1].back())) {
                                        put_from_one_column_to_other(cards_columns, first_column, second_column, number_of_cards);
                                   }
                                   else {
                                        display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                                   }

                              }
                         }
                         else if (!cards_columns[first_column - 1].empty() && cards_columns[second_column - 1].empty()) {
                              if (cards_columns[first_column - 1][cards_columns[first_column - 1].size() - number_of_cards].value == 13) {
                                   put_from_one_column_to_other(cards_columns, first_column, second_column, number_of_cards);
                              }
                              else {
                                   display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                              }
                         }
                         else {
                              display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                         }
                    }
                    else {
                         display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                    }
               }
          }
     } while (first_column != 8 && second_column != 8);
}

void put_from_one_column_to_other(std::vector<card> cards_columns[], int& first_column, int& second_column, int& number_of_cards) {
     std::stack<card> cards_buffer;
     for (int i = 0; i < number_of_cards; i++) {
          cards_buffer.push(cards_columns[first_column - 1].back());
          cards_columns[first_column - 1].pop_back();
     }
     for (int i = 0; i < number_of_cards; i++) {
          cards_columns[second_column - 1].push_back(cards_buffer.top());
          cards_buffer.pop();
     }
     if (!cards_columns[first_column - 1].empty()) {
          if (cards_columns[first_column - 1].back().visible == false) {
               cards_columns[first_column - 1].back().visible = true;
          }
     }
     first_column = 8;
}

void move_card_from_winnig_stack_to_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     int which_winnig_stack = 0;
     int column = 0;
     do {
          which_winnig_stack = display_and_choose_winnig_stack(open_deck, cards_columns, winnig_stack, mode);
          if (which_winnig_stack < 5) {
               column = display_and_choose_column(open_deck, cards_columns, winnig_stack, mode);
               if (column < 8) {
                    if (!winnig_stack[which_winnig_stack - 1].empty() && !cards_columns[column - 1].empty()) {
                         if (check_if_can_put_card(winnig_stack[which_winnig_stack - 1].top(), cards_columns[column - 1].back())) {
                              put_card_on_column_from_deck(winnig_stack[which_winnig_stack - 1], cards_columns, column);
                         }
                         else {
                              display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                         }
                    }
                    else if (!winnig_stack[which_winnig_stack - 1].empty() && cards_columns[column - 1].empty()) {
                         if (winnig_stack[which_winnig_stack - 1].top().value == 13) {
                              put_card_on_column_from_deck(winnig_stack[which_winnig_stack - 1], cards_columns, column);
                         }
                    }
                    else {
                         display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                    }
               }
          }
          else {
               system("CLS");
          }
     } while (column != 8 && which_winnig_stack != 5);
}

void move_card_from_column_to_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     int column = 0;
     int which_stack = 0;
     do {
          column = display_and_choose_column(open_deck, cards_columns, winnig_stack, mode);
          if (column < 8) {
               if (!cards_columns[column - 1].empty()) {
                    char symbol = cards_columns[column - 1].back().symbol;
                    which_stack = symbol_map_dictionary(symbol);
                    if (winnig_stack[which_stack].empty() && cards_columns[column - 1].back().value == 1) {
                         put_card_on_winnig_stack_from_column(cards_columns, winnig_stack, which_stack, column);
                    }
                    else if (!winnig_stack[which_stack].empty()) {
                         if (winnig_stack[which_stack].top().value + 1 == cards_columns[column - 1].back().value) {
                              put_card_on_winnig_stack_from_column(cards_columns, winnig_stack, which_stack, column);
                         }
                         else {
                              display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
                         }
                    }
               }
               else {
                    display_impossible_move(open_deck, cards_columns, winnig_stack, mode);
               }
          }

     } while (column != 8);
}

void put_card_on_winnig_stack_from_column(std::vector<card> cards_columns[], std::stack<card> winnig_stack[], int which_stack, int& column) {
     winnig_stack[which_stack].push(cards_columns[column - 1].back());
     cards_columns[column - 1].pop_back();
     if (!cards_columns[column - 1].empty()) {
          cards_columns[column - 1].back().visible = true;
     }
     column = 8;
}

int symbol_map_dictionary(char symbol) {
     int which_stack = 0;
     switch (symbol) {
     case 'H': {
          which_stack = 0;
          break;
     }
     case 'S': {
          which_stack = 1;
          break;
     }
     case 'D': {
          which_stack = 2;
          break;
     }
     case 'C': {
          which_stack = 3;
          break;
     }
     }
     return which_stack;
}

void move_cards_from_deck_to_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[]) {
     if (!open_deck.empty()) {
          int which_stack = symbol_map_dictionary(open_deck.top().symbol);
          if (winnig_stack[which_stack].empty() && open_deck.top().value == 1) {
               winnig_stack[which_stack].push(open_deck.top());
               winnig_stack[which_stack].top().visible = true;
               open_deck.pop();
               system("CLS");
          }
          else if (!winnig_stack[which_stack].empty()) {
               if (winnig_stack[which_stack].top().value + 1 == open_deck.top().value) {
                    winnig_stack[which_stack].push(open_deck.top());
                    winnig_stack[which_stack].top().visible = true;
                    open_deck.pop();
                    system("CLS");
               }
          }
          else {
               system("CLS");
               std::cout << "Move impossible, choose different move" << std::endl;
          }
     }
}

void restart_game() {

}

bool check_if_can_put_card(card first_card, card second_card) {
     if (second_card.value == first_card.value + 1 && second_card.color != first_card.color) {
          return true;
     }
     else {
          return false;
     }
}

void fill_cards_columns(std::vector<card> cards_columns[], std::stack <card>& deck) {
     for (int i = 1; i < 8; i++) {
          for (int j = 0; j < i; j++) {
               cards_columns[i - 1].push_back(deck.top());
               deck.pop();
          }
          cards_columns[i - 1].back().visible = true;
     }
}

int display_and_choose_column(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     char column = '0';
     do {
          std::cin.clear();
          std::cin.ignore(100, '\n');
          system("CLS");
          display_board(open_deck, winnig_stack, cards_columns, mode);
          SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
          std::cout << "Choose column (type 1 to 7 or 8):" << std::endl;
          std::cout << "1 to 7.One of the columns on the board" << std::endl;
          std::cout << "8.Back to choosing move" << std::endl;
          std::cin >> column;
     } while (column != '1' && column != '2' && column != '3' && column != '4' && column != '5' && column != '6' && column != '7' && column != '8');
     int column_int = (int)column - 48;
     system("CLS");
     return column_int;
}

int display_and_choose_winnig_stack(std::stack<card>& open_deck, std::vector<card> cards_columns[], std::stack<card> winnig_stack[], char mode) {
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
     char which_winnig_stack = '0';
     do {
          std::cin.clear();
          std::cin.ignore(100, '\n');
          system("CLS");
          display_board(open_deck, winnig_stack, cards_columns, mode);
          SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
          std::cout << "Choose winnig stack you will take card(type 1 to 4 or 5):" << std::endl;
          std::cout << "1 to 4.One of the winnig stacks on the board" << std::endl;
          std::cout << "5.Back to choosing move" << std::endl;
          std::cin >> which_winnig_stack;
     } while (which_winnig_stack != '1' && which_winnig_stack != '2' && which_winnig_stack != '3' && which_winnig_stack != '4' && which_winnig_stack != '5');
     int winnig_stack_int = (int)which_winnig_stack - 48;
     return winnig_stack_int;
}

int display_and_choose_number_of_moved_cards() {
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);
     int numbers_of_cards = 0;
     do {
          std::cin.clear();
          std::cin.ignore(100, '\n');
          std::cout << "Specify how many card you move over from choosen column:" << std::endl;
          std::cin >> numbers_of_cards;
     } while (std::cin.fail());
     return numbers_of_cards;
}

