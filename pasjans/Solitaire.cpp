#include <conio.h>
#include <iterator>
#include "game.h"

int main()
{
     CONSOLE_SCREEN_BUFFER_INFO csbi;
     GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
     std::stack <card> deck = init_deck();
     menu(deck);
     FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), csbi.wAttributes);
     return 0;
}



